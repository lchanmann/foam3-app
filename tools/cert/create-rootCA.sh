#!/usr/bin/env bash
set -e

# Generates a root certificate authority

CA=rootCA

if [ -f "$CA.crt" ] || [ -f "$CA.key" ]; then
  if [ ! -f "$CA.crt" ] || [ ! -f "$CA.key" ]; then
    echo -e "Missing public and private key pair!"
    echo -e "Please remove $CA.crt and $CA.key then try again."
    exit 1
  else
    echo -e "Certificate authority files ($CA.crt and $CA.key) already exist!"
    exit
  fi
fi

# Set certificate signing request (CSR) variables
SUBJ="
C=CA
ST=Ontario
localityName=Toronto
CN=FOAM
O=Development
organizationalUnitName=R&D
emailAddress=dev@foamdev.com
"

# Generate certificate authority private key
openssl genrsa -out "$CA.key" 4096

# Generate certificate authority public certificate
openssl req -x509 -new -nodes -subj "$(echo -n "$SUBJ" | tr "\n" "/")" -key "$CA.key" -sha256 -days 3650 -out "$CA.crt"

echo -e "Success!"
echo
echo -e "Certificate authority files created:"
echo -e "  - $CA.crt is the public certificate"
echo -e "  - $CA.key is the private key"
