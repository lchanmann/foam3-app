#!/usr/bin/env bash
set -e

# Refs
# - https://www.digicert.com/kb/ssl-support/openssl-quick-reference-guide.htm
# - https://medium.com/@leonardodna/the-ultimate-newbie-guide-for-self-signed-certificates-d81aa3b9987b
# - https://medium.com/@leonardodna/the-ultimate-newbie-guide-for-root-and-subdomain-certificates-4476d34dde27


# Generates a self-signed certificate
if [ "$#" -lt 1 ]; then
  echo "Usage: $0 <site-name> [password]"
  exit 1
fi


CA=rootCA
if [ ! -f "$CA.crt" ] || [ ! -f "$CA.key" ]; then
  ./create-rootCA.sh
fi


SITE=$1
PASSWORD=
if [ "$#" -gt 1 ]; then
  PASSWORD=$2
else
  read -s -p "Enter $SITE certificate password: " PASSWORD
fi

# Check password length
if [ "${#PASSWORD}" -lt 6 ]; then
  echo
  echo "Password must be at least 6 characters!"
  exit 1
fi

# Generate a private key
openssl genrsa -out "$SITE.key" 4096

# Create a certificate signing request
openssl req -new -key "$SITE.key" -out "$SITE.csr"

# Create the signed certificate
openssl x509 -req \
    -in "$SITE.csr" \
    -extfile alt-names.ext \
    -CA "$CA.crt" \
    -CAkey "$CA.key" \
    -out "$SITE.crt" \
    -days 3650 \
    -sha256

rm "$SITE.csr"

# Create pkcs and key store
openssl pkcs12 \
        -inkey "$SITE.key" \
        -in "$SITE.crt" \
        -export -out "$SITE.pkcs12" \
        -passout pass:"$PASSWORD"


# Create java keystore files
keytool \
    -importkeystore \
    -srckeystore "$SITE.pkcs12" \
    -srcstoretype PKCS12 \
    -srcstorepass "$PASSWORD" \
    -destkeystore "$SITE.jks" \
    -deststoretype JKS \
    -deststorepass "$PASSWORD"

keytool -list -v \
        -keystore "$SITE.jks" \
        -storepass "$PASSWORD"

cat $SITE.crt $SITE.key > $SITE.pem

echo -e "Success!"
echo
echo -e "What's next?"
echo -e "  - Copy keystore file $SITE.jks to resources/ directory."
echo -e "  - Configure keystore and password for 'http' and 'sslContextFactory' nspecs."
echo -e "  - Use $SITE.key and $SITE.crt in the web server."
echo -e "  - Use $CA.crt and $SITE.pem in load-balancer."
echo -e "  - Import $CA.crt into browser to make it accept $SITE certificate."
