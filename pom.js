const fs_   = require('fs');
const exec_ = require('child_process');
const path_ = require('path');

foam.POM({
  tasks: [
    function init(appName, version) {
      execSync('git init');

      // Remove bootstraping foam3 directory and default license file
      fs_.rmSync('./foam3', {recursive: true, force: true});
      fs_.rmSync('./LICENSE');

      // Checkout foam3 as submodule and run npm install
      execSync('git submodule add https://github.com/kgrgreer/foam3.git');
      execSync('cd foam3 && npm install');

      // Rewrite pom.js from template
      const path = path_.resolve('./pom.js');
      if ( fs_.existsSync('pom_template') ) {
        const pom = fs_.readFileSync('pom_template').toString();
        fs_.writeFileSync(path,
          pom.replaceAll('INSERT_NAME', appName)
             .replaceAll('INSERT_VERSION', version)
        );
        fs_.rmSync('pom_template');
      }
    }
  ]
});

// -----------------
// Helper functions
// -----------------
function execSync(cmd, options) {
  console.log('\x1b[0;32mExec: ' + cmd + '\x1b[0;0m');
  return exec_.execSync(cmd, options);
}
