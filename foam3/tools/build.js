#!/usr/bin/env node
// Bootstrap build script, extracted from FOAM3 tools/build.js, to provide
// funtionality for executing POM tasks eg. (./build.sh -Xinit)

var PWD = process.cwd();
var TASKS, EXPORTS;

globalThis.foam = {
  POM: function (pom) {
    TASKS = pom.tasks;
  }
};

require(PWD + '/pom.js');

var summary = [];
var depth   = 1;
var tasks   = [];
var running = {};


function task(desc, dep, f) {
  if ( arguments.length == 1 ) {
    f    = desc;
    desc = '';
    dep  = [];
  }

  if ( ! tasks[f.name] )
    tasks[f.name] = [desc, dep];

  var fired = false;
  var rec   = [ ];

  var SUPER = globalThis[f.name] || function() { };
  globalThis[f.name] = function(...args) {
    if ( fired ) return;
    fired = true;

    running[f.name] = (running[f.name] || 0) + 1;
    if ( running[f.name] === 1 ) {
      summary.push(rec);
      info(`Starting Task :: ${f.name}`);
      var start = Date.now();
      rec[0] = ''.padEnd(2*depth) + f.name;
      rec[2] = start;
      depth++;
    }

    f.bind(Object.assign({ SUPER }, EXPORTS))(...args);

    running[f.name] -= 1;
    if ( running[f.name] === 0 ) {
      depth--;
      var end = Date.now();
      var dur = ((end-start)/1000).toFixed(1);
      info(`Finished Task :: ${f.name} in ${dur} seconds`);
      rec[1] = dur;
    }
  }
}


function showSummary() {
  var s = 'Execution Summary:\n';
  summary.forEach(e => {
    if ( e[1] === undefined ) {
      var end = Date.now();
      var dur = ((end-e[2])/1000).toFixed(1);
      e[1] = dur;
    }
    s += e[0].padEnd(25) + ' ' + e[1].padStart(15) + 's\n';
  });
  info(s);
}


function quit(code) {
  showSummary();
  process.exit(code);
}


function info(msg) {
  console.log('\x1b[0;34mINFO ::', msg, '\x1b[0;0m');
}


function processArgs() {
  const args = process.argv.slice(2);
  for ( var i = 0 ; i < args.length ; i++ ) {
    var arg = args[i];
    if ( arg.startsWith('-') ) {
      for ( var j = 1 ; j < arg.length ; j++ ) {
        var a = arg.charAt(j);
        var d = ARGS[a];
        if ( d ) {
          d[1](arg.substring(j+1));
          if ( a >= 'A' && a <= 'Z' ) break;
        } else {
          console.log('Unknown argument "' + a + '"');
        }
      }
    }
  }
}


const ARGS = {
  X: [ 'Execute a list of tasks.',
    args => {
      args.split(',').forEach(t => {
        // Support build task with args eg. -XcheckDeps:5 will execute checkDeps(5)
        var s = t.split(':');
        var f = globalThis[s[0]];
        if ( f ) {
          f(...s.slice(1));
        } else {
          console.log('Unknown Command:', t);
        }
      });
      quit(0);
    } ]
};

// Install POM tasks
if ( TASKS ) {
  TASKS.forEach(f => task(f));
};

task('All', [], function all() {
  processArgs();
});


all();

quit(0);
